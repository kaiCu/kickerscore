import datetime
import random

import sqlalchemy.exc
from flask.ext.script import Manager, Shell
from kickerscore import create_app, db, Match, Player, Score
from kickerscore.elo import update_match_elo

app = create_app()
app.config['SQLALCHEMY_ECHO'] = True
manager = Manager(app)

def insert_random_data():
    two_hours = datetime.timedelta(hours=2)
    date = datetime.datetime(2011, 06, 01)
    players = []
    for player in ['Olli', 'Dodo', 'Hilmar', 'Jonas', 'Marcel', 'Kai', 'Nils']:
        p = Player(name=player)
        print 'adding', p
        db.session.add(p)
        players.append(p)
    db.session.commit()
    for _ in range(1000):
        date += two_hours
        red, blue = random.sample(players, 2)
        score_red, score_blue = random.sample([5, random.randint(0, 4)], 2)
        m = Match(red1=red, score_red=score_red, score_blue=score_blue, blue1=blue, date=date)
        db.session.add(m)
        m.insert_scores()
    db.session.commit()

def _make_context():
    return dict(app=app, db=db, Match=Match, Player=Player, Score=Score)

manager.add_command("shell", Shell(make_context=_make_context))

@manager.command
def fill_random():
    app.config['SQLALCHEMY_ECHO'] = False
    db.drop_all()
    db.create_all()
    insert_random_data()

@manager.command
def refresh_scores():
    app.config['SQLALCHEMY_ECHO'] = False
    for match in Match.query.all():
        match.remove_scores()
        match.insert_scores()
    db.session.commit()

@manager.command
def elo():
    app.config['SQLALCHEMY_ECHO'] = False
    for p in Player.query:
        p.elo_single = 0
        p.elo_def = 0
        p.elo_off = 0
        p.elo_double = 0

    for n, m in enumerate(Match.query):
        if n % 100 == 0:
            print n
        update_match_elo(m)
    db.session.commit()

@manager.command
def dump():
    import sys
    dump_to_csv(sys.stdout)

def dump_to_csv(outfile):
    import csv
    writer = csv.writer(outfile)
    writer.writerow([
        'Date', 'Player Red (Def)', 'Player Red (Off)',
        'Score Red', 'Score Blue', 'Player Blue (Def)', 'Player Blue (Off)',])

    for match in Match.query.order_by(Match.date):
        writer.writerow((
            match.date,
            match.red1.name, match.red2.name if match.red2 else '',
            match.score_red, match.score_blue,
            match.blue1.name, match.blue2.name if match.red2 else '',
        ))


@manager.command
def migrate():
    for tbl in ['elo_single', 'elo_double', 'elo_off', 'elo_def']:
        session = db.session
        try:
            session.execute("alter table players add %s integer" % tbl)
        except sqlalchemy.exc.OperationalError:
            session.rollback()
        else:
            session.commit()

@manager.command
def load():
    app.config['SQLALCHEMY_ECHO'] = False
    db.drop_all()
    db.create_all()

    players = {}
    def player(name):
        if not name:
            return None
        if name not in players:
            players[name] = Player(name)
        return players[name]

    import csv
    import sys
    import datetime
    reader = csv.DictReader(sys.stdin, ['date', 'red1', 'red2', 'score_red', 'score_blue', 'blue1', 'blue2'])
    reader.next() # skip header

    for row in reader:
        m = Match()
        row['date'] = row['date'].rsplit('.', 1)[0] # strip microseconds
        m.date = datetime.datetime.strptime(row['date'], "%Y-%m-%d %H:%M:%S")
        m.red1 = player(row['red1'])
        m.red2 = player(row['red2'])
        m.score_red = int(row['score_red'])
        m.blue1 = player(row['blue1'])
        m.blue2 = player(row['blue2'])
        m.score_blue = int(row['score_blue'])
        db.session.add(m)
        m.insert_scores()
    db.session.commit()

@manager.command
def backup():
    import datetime
    now = datetime.datetime.now()
    filename = 'kickerscore-backup-%s.csv' % now.strftime('%Y%m%d-%H%M')
    with open(filename, 'w') as f:
        dump_to_csv(f)

@manager.command
def init():
    db.create_all()
    db.session.commit()

@manager.command
def add_user(users):
    for user in users.split(','):
        if Player.by_name(user):
            print user, 'exists'
        else:
            db.session.add(Player(user))
            print user, 'created'
    db.session.commit()

if __name__ == "__main__":
    manager.run()