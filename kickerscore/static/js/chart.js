var Chart = function(options) {
    var self = this;
    self.playersContainer = "#player_choises";
    self.chartContainer = "#chart";
    self.resetZoomElement = "#reset_zoom";
    self.from = false;
    self.to = false;
    self.plotOptions = {
        xaxis: {
            mode: "time",
            timeformat: "%d. %m. %Y"
        },
        selection: {
            mode: "x"
        }
    };

    self = $.extend(true, self, options);

    $.each(self.datasets, function(key, val) {
        var element = '';
        element += '<label class="checkbox" for="id'+key+'">';
        element += '<input type="checkbox" name="' + key +'" checked="checked" id="id' + key + '"></input>';
        element += val.label + '</label>';
        $(self.playersContainer).append(element);
    });

    $(self.playersContainer + " input").click({self: self}, self.handlePlayerChoise);
    $(self.resetZoomElement).click({self: self}, self.handleZoomReset);

    var data = self.choisedPlayers();
    self.plotChart(data);
};
Chart.prototype = {
    plotChart: function(data) {
        var self = this;
        var options = $.extend(true, {}, self.plotOptions);

        if(self.from && self.to) {
            options['xaxis']['min'] = self.from;
            options['xaxis']['max'] = self.to;
            data = self._trimData(data);
        }

        $.plot(self.chartContainer, data, options);

        $(self.chartContainer).bind("plotselected", function (event, ranges) {
            self.from = ranges.xaxis.from;
            self.to = ranges.xaxis.to;
            var data = self.choisedPlayers();
            self.plotChart(data);
        });
    },
    choisedPlayers: function () {
        var self = this;
        var data = [];

        $("#player_choises input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && self.datasets[key]) {
                data.push(self.datasets[key]);
            }
        });

        return data;
    },
    handlePlayerChoise: function(event) {
        var self = event.data.self;
        var data = self.choisedPlayers();
        self.plotChart(data);
    },
    handleZoomReset: function(event) {
        var self = event.data.self;
        self.from = false;
        self.to = false;
        var data = self.choisedPlayers();
        self.plotChart(data);
    },
    // found at http://imcol.in/2012/06/jquery-flot-selection-autoscale/
    _trimData: function(data) {
        var self = this;
        if(!self.from && !self.to) {
            return data;
        }

        var newSeries = [];

        $.each(data, function(e, obj) {
            var newData = [];
            var firstIdx = false;
            var lastIdx = false;
            var firstTS = self.to;
            var lastTS = self.from;

            $.each(obj['data'], function(idx, val) {
                if ((val[0] >= self.from) && (val[0] <= self.to)) {
                    // look after timestamps cause val is touple like and
                    // $.inArray won't find it
                    if(firstTS > val[0]) {
                        firstTS = val[0];
                        firstIdx = idx;
                    }
                    if(lastTS < val[0]) {
                        lastTS = val[0];
                        lastIdx = idx;
                    }
                    newData.push(val);
                }
            });

            // add previos and past element to make graph look good
            var preIdx = firstIdx - 1;
            if(preIdx > 0) {
                newData = $.merge([obj.data[preIdx]], newData);
            }
            var pastIdx = lastIdx + 1;
            if(pastIdx > 1 && pastIdx < obj.data.length - 1) {
                newData = $.merge(newData, [obj.data[pastIdx]])
            }

            newSeries.push($.extend({}, obj, {'data': newData}));
        });
      return newSeries;
    },
};
