$(document).ready(function(){
  if (!$('#multiplayer').attr('checked')) {
    $('.player2').toggleClass('off');
    $('#switch_multiplayer').text('2 on 2');
  }

  $('#switch_multiplayer').bind('click', function() {
    $('.player2').toggleClass('off');

    if ($('.player2').first().hasClass('off')) {
      $('#multiplayer').removeAttr('checked');
      $('#switch_multiplayer').text('2 on 2');
    } else {
      $('#multiplayer').attr('checked', true);
      $('#switch_multiplayer').text('1 on 1');
    }
    return false;
  });

  var selectedPlayerIDs = [];
  var previousPlayerID;

  $.each($('.player'), function (id, playerSelect) {
    $(playerSelect).focus(function () {
        previousPlayerID = $(this).find(":selected").val();
    }).change(function (event) {
      // remove previous selected player from array
      selectedPlayerIDs = $.grep(selectedPlayerIDs, function(value) {
        return previousPlayerID != value;
      });

       // safe which player is selected
      var selectedPlayer = $(this).val();
      selectedPlayerIDs.push(selectedPlayer);
      previousPlayerID = selectedPlayer;

      $.each($('.player'), function (select_id, players) {
        // check all options
        $.each(players.options, function (player_id, name) {
          var player_value = $(name).val();
          if ($.inArray(player_value, selectedPlayerIDs) != -1) {
            $(name).attr('disabled', 'disabled');
          } else {
            $(name).removeAttr('disabled');
          }
        });
      });
    });
  });


  $('#score_form').submit(function() {
    // remove disabled before loading
    var disabledOptions = $(this).find("select>option:disabled");
    $.each(disabledOptions, function(index, option) {
      $(option).removeAttr('disabled')
    });
  });

});
