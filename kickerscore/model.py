from __future__ import division
import datetime

from collections import namedtuple, defaultdict

from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import cached_property

db = SQLAlchemy()

class Player(db.Model):
    __tablename__ = 'players'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)

    elo_single = db.Column(db.Float)
    elo_double = db.Column(db.Float)
    elo_off = db.Column(db.Float)
    elo_def = db.Column(db.Float)

    def __init__(self, name=None):
        self.name = name

    @classmethod
    def by_name(cls, name):
        if isinstance(name, cls):
            return name
        return cls.query.filter(Player.name==name).first()

    @cached_property
    def won(self):
        return Score.query.filter(Score.player==self).filter(Score.result==Score.Result.WON).count()

    @cached_property
    def lost(self):
        return Score.query.filter(Score.player==self).filter(Score.result==Score.Result.LOST).count()

    @cached_property
    def games(self):
        return Score.query.filter(Score.player==self).count()

    @cached_property
    def points(self):
        return self.won - self.lost

    @cached_property
    def ratio(self):
        if not self.games:
            return 0
        return (self.won/self.games) * 100

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<Player %r>' % self.name

class stats_counter(object):
    def __init__(self):
        self.won = 0
        self.lost = 0
        self.diff = 0
        self.points = 0
        self.games = 0

    @property
    def ratio(self):
        if not self.games:
            return 0
        return (self.won/self.games) * 100

class Statistics(object):
    def __init__(self, player=None, opponent=None, from_date=None, to_date=None, single=True, double=True):
        self.player = Player.by_name(player)
        self.opponent = Player.by_name(opponent)
        self.month = defaultdict(lambda: defaultdict(stats_counter))
        self.week = defaultdict(lambda: defaultdict(stats_counter))
        self.total = defaultdict(stats_counter)

        query = Score.query
        if player:
            query = query.filter(Score.player==self.player)
        if opponent:
            query = query.filter(Score.opponent==self.opponent)
        if from_date:
            query = query.filter(Score.date >= from_date)

        if single and not double:
            query = query.filter(Score.teammate==None)
        if double and not single:
            query = query.filter(Score.teammate!=None)

        for score in query:
            self._update_stats(self.month[score.player][(score.date.year, score.date.month)], score)
            self._update_stats(self.week[score.player][(score.date.year, score.date.isocalendar()[1])], score)
            self._update_stats(self.total[score.player], score)

    def _update_stats(self, stats, score):
        stats.games += 1
        stats.diff += score.score - score.score_opponent
        if score.won:
            stats.won += 1
            stats.points += 1
        if score.lost:
            stats.lost += 1
            stats.points -= 1

    def table(self):
        rows = [(p, s) for p, s in self.total.iteritems()]
        rows.sort(key=lambda x: (x[1].points, x[1].ratio), reverse=True)
        return rows

class TeamStatistics(object):
    def __init__(self, from_date=None, with_positions=False):
        self.red = defaultdict(stats_counter)
        self.blue = defaultdict(stats_counter)
        self.total = defaultdict(stats_counter)

        query = Match.query.filter(Match.score_red > Match.score_blue)
        query = query.filter(Match.red2 != None)
        if from_date:
            query = query.filter(Match.date >= from_date)

        for match in query:
            if with_positions:
                team_red = (match.red1, match.red2)
                team_blue = (match.blue1, match.blue2)
            else:
                team_red = (min(match.red1, match.red2), max(match.red1, match.red2))
                team_blue = (min(match.blue1, match.blue2), max(match.blue1, match.blue2))
            self._update_stats(self.total[team_red], match, won=True)
            self._update_stats(self.total[team_blue], match, won=False)

        query = Match.query.filter(Match.score_red < Match.score_blue)
        query = query.filter(Match.red2 != None)
        if from_date:
            query = query.filter(Match.date >= from_date)

        for match in query:
            if with_positions:
                team_red = (match.red1, match.red2)
                team_blue = (match.blue1, match.blue2)
            else:
                team_red = (min(match.red1, match.red2), max(match.red1, match.red2))
                team_blue = (min(match.blue1, match.blue2), max(match.blue1, match.blue2))
            self._update_stats(self.total[team_red], match, won=False)
            self._update_stats(self.total[team_blue], match, won=True)

    def _update_stats(self, stats, match, won):
        stats.games += 1
        diff = abs(match.score_red - match.score_blue)
        stats.diff += diff if won else -diff
        if won:
            stats.won += 1
            stats.points += 1
        else:
            stats.lost += 1
            stats.points -= 1

    def table(self):
        rows = [(p, s) for p, s in self.total.iteritems()]
        rows.sort(key=lambda x: (x[1].points, x[1].ratio, x[1].diff), reverse=True)
        return rows

class OpponentStatistics(object):
    def __init__(self, from_date=None, defense=False):
        self.total = defaultdict(stats_counter)

        query = Match.query.filter(Match.red2 != None)
        if from_date:
            query = query.filter(Match.date >= from_date)

        for match in query:
            if defense:
                opponents_a = (match.red1, match.blue2)
                opponents_b = (match.blue1, match.red2)
            else:
                opponents_a = (match.red2, match.blue1)
                opponents_b = (match.blue2, match.red1)

            self._update_stats(self.total[opponents_a], match, won=match.score_red > match.score_blue)
            self._update_stats(self.total[opponents_b], match, won=match.score_blue > match.score_red)

    def _update_stats(self, stats, match, won):
        stats.games += 1
        diff = abs(match.score_red - match.score_blue)
        stats.diff += diff if won else -diff
        if won:
            stats.won += 1
            stats.points += 1
        else:
            stats.lost += 1
            stats.points -= 1

    def table(self):
        rows = self.total.items()
        rows.sort(key=lambda x: (x[1].points, x[1].ratio, x[1].diff), reverse=True)
        return rows


class Score(db.Model):
    __tablename__ = 'scores'
    class Position(object):
        BOTH = 0
        DEF = 1
        OFF = 2

    class Side(object):
        RED = 0
        BLUE = 1

    class Result(object):
        LOST = -1
        TIE = 0
        WON = 1

    id = db.Column(db.Integer, primary_key=True)
    match_id = db.Column(db.Integer, db.ForeignKey('matches.id'))
    match = db.relationship('Match', primaryjoin='Score.match_id==Match.id', lazy='lazy', backref='scores')
    player_id = db.Column(db.Integer, db.ForeignKey('players.id'), index=True)
    player = db.relationship('Player', backref='scores',
        primaryjoin='Score.player_id==Player.id', lazy='joined')
    teammate_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    teammate = db.relationship('Player', primaryjoin='Score.teammate_id==Player.id', lazy='joined')
    opponent_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    opponent = db.relationship('Player', primaryjoin='Score.opponent_id==Player.id', lazy='joined')
    opponent_teammate_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    opponent_teammate = db.relationship('Player', primaryjoin='Score.opponent_teammate_id==Player.id', lazy='joined')
    score = db.Column(db.SmallInteger, nullable=False)
    score_opponent = db.Column(db.SmallInteger, nullable=False)
    result = db.Column(db.SmallInteger, nullable=False)
    position = db.Column(db.SmallInteger)
    side = db.Column(db.SmallInteger)
    date = db.Column(db.DateTime, index=True)

    @property
    def won(self):
        return self.result == Score.Result.WON

    @property
    def lost(self):
        return self.result == Score.Result.LOST

    def __repr__(self):
        return '<Score %s/%s %d:%d %s/%s on %s>' % (
            self.player, self.teammate, self.score, self.score_opponent,
            self.opponent, self.opponent_teammate, self.date
        )

class Match(db.Model):
    __tablename__ = 'matches'

    id = db.Column(db.Integer, primary_key=True)
    red1_id = db.Column(db.Integer, db.ForeignKey('players.id'), nullable=False)
    red2_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    blue1_id = db.Column(db.Integer, db.ForeignKey('players.id'), nullable=False)
    blue2_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    # off or single player
    red1 = db.relationship('Player', primaryjoin='Match.red1_id==Player.id', lazy='joined')
    # def
    red2 = db.relationship('Player', primaryjoin='Match.red2_id==Player.id', lazy='joined')
    # off or single player
    blue1 = db.relationship('Player', primaryjoin='Match.blue1_id==Player.id', lazy='joined')
    # def
    blue2 = db.relationship('Player', primaryjoin='Match.blue2_id==Player.id', lazy='joined')
    score_red = db.Column(db.Integer, nullable=False)
    score_blue = db.Column(db.Integer, nullable=False)
    date = db.Column(db.DateTime, default=datetime.datetime.now, index=True)

    def insert_scores(self):
        result_red = Score.Result.WON if self.score_red > self.score_blue else Score.Result.LOST
        result_blue = Score.Result.WON if self.score_blue > self.score_red else Score.Result.LOST

        s = Score(match=self, player=self.red1, teammate=self.red2,
            opponent=self.blue1, opponent_teammate=self.blue2,
            score=self.score_red, score_opponent=self.score_blue,
            position=Score.Position.DEF if self.red2 else Score.Position.BOTH,
            side=Score.Side.RED, date=self.date, result=result_red)
        db.session.add(s)
        s = Score(match=self, player=self.blue1, teammate=self.blue2,
            opponent=self.red1, opponent_teammate=self.red2,
            score=self.score_blue, score_opponent=self.score_red,
            position=Score.Position.DEF if self.blue2 else Score.Position.BOTH,
            side=Score.Side.BLUE, date=self.date, result=result_blue)
        db.session.add(s)
        if self.red2:
            s = Score(match=self, player=self.red2, teammate=self.red1,
                opponent=self.blue1, opponent_teammate=self.blue2,
                score=self.score_red, score_opponent=self.score_blue,
                position=Score.Position.OFF,#,  if not self.red_changed else Score.Position.BOTH,
                side=Score.Side.RED, date=self.date, result=result_red)
            db.session.add(s)
            s = Score(match=self, player=self.blue2, teammate=self.blue1,
                opponent=self.red1, opponent_teammate=self.red2,
                score=self.score_blue, score_opponent=self.score_red,
                position=Score.Position.OFF,# if not self.blue_changed else Score.Position.BOTH,
                side=Score.Side.BLUE, date=self.date, result=result_blue)
            db.session.add(s)

    def __repr__(self):
        return 'Match(%s/%s %d:%d %s/%s)' % (
            self.blue1.name, self.blue2 and self.blue2.name,
            self.score_blue, self.score_red,
            self.red1.name, self.red2 and self.red2.name,
            )

    def remove_scores(self):
        Score.query.filter(Score.match==self).delete()
