from __future__ import division
import datetime
import itertools
import os
import warnings
import operator
import time

import flask
from flask import Flask, render_template, request, redirect, url_for, flash, jsonify
from flask.ext.wtf import Form
from wtforms.fields import SelectField, BooleanField, DateTimeField, TextField
from wtforms.validators import Required, ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from kickerscore.elo import update_match_elo
from kickerscore.model import db, Player, Score, Match, Statistics, TeamStatistics, OpponentStatistics
from kickerscore.util import datetime_ago, format_rel_datetime, format_datetime

app = Flask('kickerscore')

def create_app(conf=None):
    app.config.from_object('kickerscore.default_settings')
    app.config.from_object(conf)
    if not conf:
        if 'KICKERSCORE_SETTINGS' in os.environ:
            app.config.from_envvar('KICKERSCORE_SETTINGS')
        else:
            warnings.warn('environment KICKERSCORE_SETTINGS not found')
    db.init_app(app)
    app.jinja_env.filters['format_rel_datetime'] = format_rel_datetime
    app.jinja_env.filters['format_datetime'] = format_datetime
    return app

def players():
    query = db.session.query(Player)
    query = query.outerjoin(Player.scores).group_by(Player)
    query = query.order_by(db.desc(Score.date), Player.name)
    return query

def player_name_unique(form,field):
    if Player.query.filter(Player.name == field.data).first():
        raise ValidationError('Player with this name already exists.')

class PlayerForm(Form):
    name = TextField('Name', validators=[Required(), player_name_unique])

class MatchForm(Form):
    red1 = QuerySelectField('Player red', query_factory=players, allow_blank=True, validators=[Required()])
    red2 = QuerySelectField('Player red2', query_factory=players, allow_blank=True)
    blue1 = QuerySelectField('Player blue', query_factory=players, allow_blank=True, validators=[Required()])
    blue2 = QuerySelectField('Player blue2', query_factory=players, allow_blank=True)
    score_red = SelectField('Score red', coerce=int, choices=[(i, i) for i in range(10,0-1,-1)])
    score_blue = SelectField('Score blue', coerce=int, choices=[(i, i) for i in range(10,0-1,-1)])
    multiplayer = BooleanField('Multiplayer')
    date = DateTimeField('Date', default=datetime.datetime.now, validators=[Required()])

@app.route("/")
def index():
    total_matches = Match.query.count()
    last_match = Match.query.order_by(db.desc(Match.date)).first()
    stats_single = Statistics(single=True, double=False, from_date=datetime_ago(days=28))
    stats_double = Statistics(single=False, double=True,from_date=datetime_ago(days=28))
    return render_template('index.html', total_matches=total_matches, last_match=last_match, stats_single=stats_single, stats_double=stats_double)

@app.route("/player/add", methods=['GET', 'POST'])
def add_player():
    form = PlayerForm()
    if form.validate_on_submit():
        p = Player(name=form.data['name'])
        db.session.add(p)
        db.session.commit()
        flash('Added player')
        return redirect(url_for('index'))
    return render_template('player_add.html', form=form)

@app.route("/table/")
@app.route("/table/<int:n>/<interval>/")
def table(interval='days', n=-1):
    games = request.args.get('games', 'all')
    if games == 'single':
        single = True
        double = False
    elif games == 'double':
        single = False
        double = True
    else:
        games = None
        single = True
        double = True

    if interval not in ('weeks', 'week', 'days', 'day'):
        raise flask.NotFound()
    if n == -1:
        stats = Statistics(single=single, double=double)
        n_text = 'bazillion'
        n = None
        interval = 'days'
    else:
        interval_key = interval if interval[-1] == 's' else interval + 's'
        stats = Statistics(from_date=datetime_ago(**{interval_key:n}), single=single, double=double)
        n_text = n

    return render_template('table.html', statistics=stats, interval=interval, n_text=n_text, n=n, games=games)

@app.route("/table/teams")
@app.route("/table/teams/<int:n>/<interval>/")
def table_teams(interval='days', n=-1):
    if interval not in ('weeks', 'week', 'days', 'day'):
        raise flask.NotFound()
    if n == -1:
        team_stats = TeamStatistics()
        n_text = 'bazillion'
        n = None
        interval = 'days'
    else:
        interval_key = interval if interval[-1] == 's' else interval + 's'
        team_stats = TeamStatistics(from_date=datetime_ago(**{interval_key:n}))
        n_text = n

    return render_template('table_teams.html', team_statistics=team_stats,
        interval=interval, n_text=n_text, n=n)

@app.route("/table/opponents")
@app.route("/table/opponents/<int:n>/<interval>/")
def table_opponents(interval='days', n=-1):
    if interval not in ('weeks', 'week', 'days', 'day'):
        raise flask.NotFound()
    defense = bool(request.args.get("defense", False))
    if n == -1:
        oppon_stats = OpponentStatistics(defense=defense)
        n_text = 'bazillion'
        n = None
        interval = 'days'
    else:
        interval_key = interval if interval[-1] == 's' else interval + 's'
        oppon_stats = OpponentStatistics(from_date=datetime_ago(**{interval_key:n}), defense=defense)
        n_text = n

    return render_template('table_opponents.html', opponent_statistics=oppon_stats,
        interval=interval, n_text=n_text, n=n, defense=defense)

@app.route("/elo/")
def elo():
    players = db.session.query(Player, db.func.count(Score.id)).outerjoin(Score, Score.player_id==Player.id).group_by(Player.id)
    players = [p for p, count in players if count > 50]
    elos = {}
    for elo in ['elo_single', 'elo_double', 'elo_off', 'elo_def']:
        players.sort(key=operator.attrgetter(elo), reverse=True)
        elos[elo] = [(i+1, p, int(getattr(p, elo))) for i, p in enumerate(players)]
    return render_template('elo.html', **elos)

@app.route("/players/<name>")
def player(name):
    p = Player.query.filter(Player.name==name).first_or_404()
    scores = Score.query.options(db.joinedload(Score.match)).filter(Score.player==p).order_by(db.desc(Score.date)).limit(100).all()
    date_scores = itertools.groupby(scores, lambda x: x.date.date())
    stats = Statistics(p, from_date=datetime_ago(days=30))
    return render_template('player.html', player=p,
        date_scores=date_scores, statistics=stats)

@app.route("/scores")
def scores():
    offset = int(request.args.get('offset', 0))
    limit = 50
    matches = Match.query.order_by(db.desc(Match.date)).offset(offset).limit(limit)

    matches = list(matches)
    date_scores = itertools.groupby(matches, lambda x: x.date.date())
    return render_template('scores.html', date_scores=date_scores,
        offset=offset, limit=limit)

@app.route("/scores/add", methods=['GET', 'POST'])
def add_score():
    form = MatchForm(request.form)
    if form.data['multiplayer']:
        form.red2.validators=[Required()]
        form.blue2.validators=[Required()]

    if form.validate_on_submit():
        if not update_score(form=form):
            return render_template('score_add.html', form=form)
        db.session.commit()
        flash('Added score')
        return redirect(url_for('add_score'))
    else:
        matches = Match.query.order_by(db.desc(Match.date)).limit(5)
        if not form.errors:
            # use multiplayer from last game if form is fresh
            multiplayer = matches[0].blue2 is not None
            form.multiplayer.checked = multiplayer
        return render_template('score_add.html', form=form, matches=matches)


@app.route("/scores/edit/<int:match_id>", methods=['GET', 'POST'])
def edit_score(match_id):
    form = MatchForm(request.form)
    matches = Match.query.order_by(db.desc(Match.date)).limit(5)
    match = Match.query.filter(Match.id == match_id).first_or_404()
    if match not in matches:
        flash('Edit not allowed')
        return redirect(url_for('add_score'))

    if form.data['multiplayer']:
        form.red2.validators=[Required()]
        form.blue2.validators=[Required()]

    if request.form.get('delete') == 'true':
        match.remove_scores()
        db.session.delete(match)
        db.session.commit()
        flash('Removed score')
        return redirect(url_for('add_score'))

    if form.validate_on_submit():
        if not update_score(form=form, match=match):
            return render_template('score_edit.html', form=form, match=match)
        db.session.commit()
        flash('Edited score')
        return redirect(url_for('add_score'))
    else:
        if not form.errors:
            form.multiplayer.checked = True if match.red2 else False
            form.red1.data = match.red1
            form.red2.data = match.red2
            form.blue1.data = match.blue1
            form.blue2.data = match.blue2
            form.score_blue.data = match.score_blue
            form.score_red.data = match.score_red
            form.date.data = match.date

        return render_template('score_edit.html', form=form, match=match)

@app.route("/elo_preview")
def elo_preview():
    red1 = Player.by_name(request.args['red1'])
    blue1 = Player.by_name(request.args['blue1'])

    red2 = Player.by_name(request.args.get('red2'))
    blue2 = Player.by_name(request.args.get('blue2'))

    match = Match(red1=red1, blue1=blue1, red2=red2, blue2=blue2,
        score_red=int(request.args['score_red']), score_blue=int(request.args['score_blue']))
    db.session.add(match)

    elos = {}
    players = []
    for p in [red1, blue1, red2, blue2]:
        if not p:
            continue
        players.append(p)
        elos[p.name] = {
            "before": {
                "single": p.elo_single,
                "double": p.elo_double,
                "def": p.elo_def,
                "off": p.elo_off,
            }
        }
    match.insert_scores()
    update_match_elo(match)

    for p in players:
        elos[p.name]["after"] = {
            "single": p.elo_single,
            "double": p.elo_double,
            "def": p.elo_def,
            "off": p.elo_off,
        }
        elos[p.name]["diff"] = {
            "single": p.elo_single - elos[p.name]["before"]["single"],
            "double": p.elo_double - elos[p.name]["before"]["double"],
            "def": p.elo_def - elos[p.name]["before"]["def"],
            "off": p.elo_off - elos[p.name]["before"]["off"],
        }

    db.session.rollback()

    return jsonify(**elos)

def update_score(form, match=None):
    red1 = form.data['red1']
    blue1 = form.data['blue1']
    multiplayer = form.data['multiplayer']
    date = form.data['date']
    date_delta = datetime.datetime.now() - date
    if date_delta.days >= 7 or date_delta.days < 0:
        flash('Date not possible - No future and not more than 7 days in the history')
        return False

    if multiplayer:
        red2 = form.data['red2']
        blue2 = form.data['blue2']
    else:
        red2 = blue2 = None

    total_players = len(set([red1, blue1, red2, blue2]) - set([None]))
    if multiplayer and total_players != 4 or not multiplayer and total_players != 2:
        flash('Player repeated')
        return False

    if match:
        match.red1 = red1
        match.red2 = red2
        match.blue1 = blue1
        match.blue2 = blue2
        match.score_red = form.data['score_red']
        match.score_blue = form.data['score_blue']
        match.date = date
        match.remove_scores()
        match.insert_scores()
    else:
        match = Match(red1=red1, blue1=blue1, red2=red2, blue2=blue2,
            score_red=form.data['score_red'], score_blue=form.data['score_blue'],
            date=date)
        db.session.add(match)
        match.insert_scores()
        update_match_elo(match)

    return True

@app.route("/chart/")
@app.route("/chart/<games_type>")
def chart(games_type='all'):
    chart = {}
    players = Player.query.all()
    for i, player in enumerate(players):
        day = False
        data = {
            'label': player.name,
            'color': i,
            'data': []
        }
        scores = Score.query.filter(Score.player==player)
        if games_type == 'single':
            scores = Score.query.filter(Score.player==player).filter(Score.teammate==None)
        elif games_type == 'double':
            scores = Score.query.filter(Score.player==player).filter(Score.teammate!=None)

        points = 0
        for i, score in enumerate(scores.all()):
            # collect points
            if score.won:
                points += 1
            elif score.lost:
                points -= 1

            # init to first score date
            if i == 0:
                day = (score.date.year, score.date.month, score.date.day, 0, 0, 0, 0, 0, 0)
            # current score date
            t = (score.date.year, score.date.month, score.date.day, 0, 0, 0, 0, 0, 0)
            # sum points per day
            if t == day:
                continue
            data['data'].append([time.mktime(day) * 1000, points])
            day = t
        chart[player.name] = data
    return render_template('chart.html', chart=chart, games_type=games_type)

if __name__ == "__main__":
    # db.create_all()
    # fill_random()
    app = create_app()
    app.run()
